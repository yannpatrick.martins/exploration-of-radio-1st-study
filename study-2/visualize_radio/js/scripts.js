
const keys = ["Signal_type", "Description", "Frequency", "Mode", "Modulation", "Bandwidth", "Location", "Sample_Audio", "Waterfall_image", "features", "bmus"]


const paths = {
  // fingerprint data ⬇️
  fma_small: {
    data: "../data/df_small_data.json",
    cb: "../data/df_small_codebook.json",
    project: "../data/df_radio_data.json"
  },
  fma_small_entropy: {
    data: "../data/df_fma_entropy_data.json",
    cb: "../data/df_fma_entropy_codebook.json",
    project: "../data/df_fma_entropy_radio.json"
  },
  topics: './js/topics.json'
}


const path = paths.fma_small_entropy
const gutter = 25
let grid = 50
let data
let projection_data
let cell_size = 10
let cnv
let loaded = false


const load_data = path => {
  console.log(path)
  fetch(path)
    .then(res => res.json())
    .then(res => init_menu(res))
    .catch(err => console.log(err))
}

const load_projection_data = path => {
  console.log(path)
  fetch(path)
    .then(res => res.json())
    .then(res => init_projection_data(res))
    .catch(err => console.log(err));
}

const imgs = []

const init_menu = (res) => {
  data = res
  draw_grid_background()
  load_topics(data)
}

let topics_loaded = false
const load_topics = (res) => {
  // here we need toe xtract the the genre and how they 
  // are distributed according to the bmus

  // we create a index array
  const indexes = []
  for (let i = 0; i < grid[0] * grid[1]; i++) {
    indexes.push(i)
  }

  // than we query the data according to the indexes
  const result = []
  for (let i = 0; i < indexes.length; i++) {
    const bmu = indexes[i]
    const queried_data_indexes = data['bmus'].map((element, index) => element[0] == bmu ? index : undefined).filter(x => x)
    let genres = []
    for (const index of queried_data_indexes) {
      genres.push(data['Genre'][index])
    }
    /**
        * FROM Stackoverflow
        * https://stackoverflow.com/questions/17313268/idiomatically-find-the-number-of-occurrences-a-given-value-has-in-an-array
        */
    const count = {}
    const dataset = genres

    dataset.forEach(function (el) {
      count[el] = count[el] + 1 || 1
    });

    result.push(
      {
        bmu,
        genres: count
      }
    )
  }

  // here we extract the genre with higher count for each cell
  const max_genre_per_cell = []
  for (let i = 0; i < result.length; i++) {
    const item = result[i]
    const genres = item.genres
    let max = 0
    let max_genre = ''
    Object.keys(genres).forEach(key => {
      const value = genres[key]
      if (value >= max) {
        max = value
        max_genre = key
      }
    })

    max_genre_per_cell.push(
      {
        bmu: item.bmu,
        genre: max_genre
      }
    )

  }

  // console.log(result)
  // console.log(max_genre_per_cell.filter(item => item.genre === 'Pop').map(item => item.bmu))


  const count = {}
  const dataset = data['Genre']

  dataset.forEach(function (el) {
    count[el] = count[el] + 1 || 1
  });
  const list_of_genres = Object.keys(count)

  console.log(list_of_genres)
  const genres_colors = ['#3f3', '#ff3', '#3ff', '#33f', '#f33', '#333', '#af3', '#fff'] // this should be a dynamic function generating colors
  const left_menu = document.querySelector('.menu-left')
  for (let i = 0; i < list_of_genres.length; i++) {
    const topic = list_of_genres[i]
    // console.log(typeof topic)
    // create element
    // const keywords = Object.keys(topic)[0]
    const key_div = document.createElement('div')
    key_div.setAttribute('class', 'menu-left-item')
    key_div.textContent = topic
    key_div.style.textDecoration = 'underline'
    key_div.style.color = genres_colors[i]
    const radio_signals_to_show = max_genre_per_cell.filter(item => item.genre === topic).map(item => item.bmu)
    key_div.addEventListener('click', () => display_topics(radio_signals_to_show, genres_colors[i]))

    left_menu.appendChild(key_div)
  }

  topics_loaded = true
}



let converted_projection_data
let projection_data_imgs = []
let imgs_loaded_in_array = false
let _100_percent = 0
let show_perc = false
const init_projection_data = res => {
  console.log(res)
  projection_data = res
  //////////////////////
  // load test data
  load_data(path.data)
  ///////////////////////

  // convert projection data
  converted_projection_data = convert_data(res)
  console.log(converted_projection_data);
  _100_percent = converted_projection_data['imgs_path_low'].length
  show_perc = true
  for (let i = 0; i < converted_projection_data['imgs_path_low'].length; i++) {
    // const path = converted_projection_data['imgs_path_low'][i].replace('signals_archive', '../data')
    const path = `../../study-1/notebooks/${converted_projection_data['imgs_path_low'][i]}`
    projection_data_imgs.push(path)
  }
  // converted_projection_data
  imgs_loaded_in_array = true
  display_grid_projection(converted_projection_data['projection-bmus'])
}

function show_all_radio() {

  display_grid_projection(converted_projection_data['projection-bmus'])
}

const display_grid_projection = (bmus, index) => {
  if (bmus) {
    noStroke()
    /**
     * concat array of arrays following solution from 
     * https://stackoverflow.com/questions/54463977/concat-arrays-in-array-of-objects
     */
    // console.log(concat_bmus);
    const loc = bmus_to_xy(bmus)
    // different color for different bmu
    // fill(0, 255, 0)

    // stroke(0)
    colorMode(HSL, 360, 100, 100, 1)
    const inc = 1 / loc.coords.length
    let val = 0
    let i = 0

    const projection_container = document.querySelector('.projection')
    projection_container.innerHTML = ''
    for (const coord of loc.coords) {
      const path = projection_data_imgs[i]
      create_images(path, coord, projection_container)
      // image(imgs[path], (coord.x * cell_size) + margin, cell_size / 4 + (coord.y * cell_size), w, h)

      val += inc
      i++
    }
    colorMode(RGB)
  }
}


const convert_data = data => {
  let result = {}
  if (data['bmus']) {
    // this workaround is in place to accomodate 
    // problematic data of fingerprints
    result['bmus'] = data['bmus'].map(val => val[0])
  }
  const ignore = 'fingerprints'
  const _1d_arr_fields = ['Signal_type', 'Description', 'Frequency', 'Mode', 'Modulation', 'Bandwidth', 'Location', 'Sample_audio', 'Waterfall_image']
  const _2d_arr_fields = ['imgs_path_high', 'imgs_path_low', 'chunks']
  for (key of _1d_arr_fields) result[key] = []
  for (key of _2d_arr_fields) result[key] = []

  let index = 0
  for (let i = 0; i < data['chunks'].length; i++) {
    const chunks = data['chunks'][i]
    for (let j = 0; j < chunks.length; j++) {
      for (const key of _2d_arr_fields) {
        //////////change this at some point
        const root = ''
        //////////////////////////////////
        result[key].push(data[key][i][j])
      }
      for (const key of _1d_arr_fields) {
        result[key].push(data[key][i])
      }
      index++
    }
  }
  result['projection-bmus'] = data["bmus-proj"]
  return result
}

const load_codebook = (res) => {
  // console.log(res)
  grid = res.size

  init_canvas(grid);

  load_projection_data(path.project)

}

const range = (start, end) => {
  const result = []
  for (let i = start; i <= end; i++) result.push(i)
  return result
}

let cnv_sizes

function init_canvas(grid) {
  cell_size = (get_bigger_dimension() - (2 * gutter)) / grid[0];
  resizeCanvas(cell_size * grid[0], cell_size * grid[1]);
  cnv_sizes = cnv.canvas.parentElement.getBoundingClientRect()
}

// function preload() {
//   imgs.push(loadImage('assets/laDefense.jpg'))
// }
function setup() {
  cnv = createCanvas(100, 100)
  cnv.parent('p5')
  background(255)

  noStroke()
  // frameRate(25)
  // noLoop()
  // const path = `data/${dir}/${namespace}/${namespace}_codebook.json`
  // const path = `data/${dir}/${namespace}_codebook.json`
  // const path = paths._2_5s.cb
  // console.log(path)

  /**
   * cascading series of fetching
   * radio codebook => project-radio-data => data
   */
  fetch(path.cb)
    .then(res => res.json())
    .then(res => load_codebook(res))
    .catch(err => console.log(err))
}



let data_loaded = false
const perc = document.querySelector('#perc')
function draw() {
  // if (loaded) {
  //   print('loaded')
  //   noLoop()
  // }

  if (imgs_loaded_in_array === true && image_loader_counter >= projection_data_imgs.length) {
    data_loaded = true
  }

  if (show_perc) {
    const value = floor((image_loader_counter / _100_percent) * 100)

    // perc.textContent = `${floor(image_loader_counter)} of ${_100_percent}`
    perc.textContent = `${value}`
  }

  if (data_loaded && topics_loaded) {
    // if (imgs.length === data['imgs_path'].length) {
    // read_inputs()
    // here do spmething to remove intro animation
    document.querySelector('.loading').style.display = 'none'
    console.log('loaded')
    noLoop();
    // }
  }
}

const tooltip_help = document.querySelector('.tooltip-help')
let show_tooltip = false
let enter = false
let exit = true
function mouseMoved() {
  const x = mouseX
  const y = mouseY
  if (edge(x, y)) {
    // need to fix this hardcoded 275 with
    tooltip_help.style.left = `${275 + x}px`
    tooltip_help.style.top = `${y}px`

    const col_center = floor(x / (cell_size))
    const row_center = floor(y / (cell_size))
    if (dist(x, y,
      (col_center * cell_size) + (cell_size / 2),
      (row_center * cell_size) + (cell_size / 2)) < cell_size / 3) {
      tooltip_help.style.visibility = 'visible'

      if (!enter) {
        /**
         * from stack overflow 
         * Return index value from filter method javascript
         * https://stackoverflow.com/questions/26468557/return-index-value-from-filter-method-javascript
         * 
        */
        const bmu = xy_to_bmu(col_center, row_center)
        const indexes_to_tooltip = data['bmus'].map((element, index) => element[0] == bmu ? index : undefined).filter(x => x)
        populate_tooltip(indexes_to_tooltip)
      }

      enter = true
    } else {
      tooltip_help.style.visibility = 'hidden'
      enter = false
    }
  }
}

const populate_tooltip = indexes => {
  tooltip_help.innerHTML = ''
  const loop = indexes.length < 5 ? indexes.length : 5
  for (let i = 0; i < loop; i++) {
    const index = indexes[i]
    const title = data['Info'][index]
    const genre = data['Genre'][index]
    const image_path = `../data/imgs/low/${data['id'][index]}.png`

    const content = document.createElement('div')
    content.setAttribute('class', 'tooltip-content')

    const img = document.createElement('img')
    img.src = image_path

    content.appendChild(img)

    const h6 = document.createElement('h6')
    h6.textContent = genre

    content.appendChild(h6)

    const p = document.createElement('p')
    p.textContent = title

    content.appendChild(p)

    tooltip_help.appendChild(content)

  }
}

const main_container = document.querySelector('.main')
const display_topics = (bmus, color) => {
  // indexes_to_show = index_arr

  draw_grid_background()
  main_container.innerHTML = ''
  if (bmus) {
    noStroke()
    const loc = bmus_to_xy(bmus)

    for (const coord of loc.coords) {
      // const path = `../../study-1/notebooks/${converted_projection_data['imgs_path_low'][index]}`
      create_image_color(color, coord, main_container)
    }
  }

}

let unique_colors_computed = false
let colors = []
const unique_colors = (length) => {
  if (unique_colors_computed) {
    return
  }

  const increment = 360 / length
  let h = 0
  for (let i = 0; i < length; i++) {
    // H = 0 – 360
    // S = 0 – 100
    // L = 0 – 100
    h += increment
    // const l = i % 2 === 0 ? 20 : 80
    const l = 50
    const s = 100
    colors.push({ h, s, l })
  }

  unique_colors_computed = true
}

const display_grid = (bmus, type, index) => {


}

const foreground_bmus = query => {


  const foreground_div = document.querySelector('.foreground')
  foreground_div.innerHTML = ''

  noStroke();
  // const queried_data = converted_projection_data['Signal_type'].map((element, index) => element === query ? index : undefined).filter(x => x)

  const queried_data = []
  for (let i = 0; i < converted_projection_data['Signal_type'].length; i++) {
    const signal_name = converted_projection_data['Signal_type'][i]
    if (signal_name === query) {
      queried_data.push(i)
    }
  }

  for (let i = 0; i < queried_data.length; i++) {
    const index = queried_data[i]
    const bmu = converted_projection_data['projection-bmus'][index]
    const path = `../../study-1/notebooks/${converted_projection_data['imgs_path_low'][index]}`
    const loc = bmus_to_xy(bmu)
    create_images(path, loc.coords[0], foreground_div, true)
  }

}

const read_inputs = () => {
  console.log('inputs read!');
  const inputs = document.querySelectorAll('.playlist input')
  const titles = []
  inputs.forEach(input => {

    if (input.checked) {
      console.log(input)
      titles.push(input.dataset.title)
    }
  })
  let bmus = []
  for (let i = 0; i < titles.length; i++) {
    const title = titles[i]
    for (let j = 0; j < converted_projection_data['Signal_type'].length; j++) {
      const signal_type = converted_projection_data['Signal_type'][j]
      if (title === signal_type) {
        console.log(signal_type, j)
        bmus.push(converted_projection_data['projection-bmus'][j])
      }
    }
    // bmus = bmus.concat(range(start, end))
  }
  // display_bmus(bmus)
  console.log(bmus)
  display_grid_projection(bmus)
}

let image_loader_counter = 0

const create_images = (path, coord, div, outline) => {
  // console.log(path)
  const left_margin = cnv_sizes.x
  const w = cell_size * 0.85
  const h = w / 2
  const margin = (cell_size - w) / 2
  const img = document.createElement('img')
  img.src = path
  img.style.position = 'fixed'
  img.style.left = left_margin + ((coord.x * cell_size) + margin) + 'px'
  img.style.top = cell_size / 4 + (coord.y * cell_size) + 'px'
  img.style.width = w + 'px'
  img.style.height = h + 'px'
  if (outline) {
    img.style.outline = '4px solid'
    img.style.outlineColor = '#55ff55'
  }


  // when image is loaded increment a conter that tells whether all images have been loaded
  img.onload = () => {
    image_loader_counter++
  }
  div.appendChild(img)
  // for
}

const create_image_color = (color, coord, div) => {
  const left_margin = cnv_sizes.x
  const w = cell_size * 0.9
  const h = cell_size * 0.9
  // const margin = (cell_size - w) / 2
  const color_img = document.createElement('div')
  color_img.style.background = color + 'a'
  color_img.style.position = 'fixed'
  color_img.style.left = 275 + (coord.x * cell_size) + ((cell_size * 0.1) / 2) + 'px'
  color_img.style.top = (coord.y * cell_size) + ((cell_size * 0.1) / 2) + 'px'
  color_img.style.width = w + 'px'
  color_img.style.height = h + 'px'
  color_img.style.borderRadius = '5px'
  // if (outline) {
  //   color_img.style.outline = '4px solid'
  //   color_img.style.outlineColor = '#55ff55'
  // }
  div.appendChild(color_img)
}

const bmus_to_xy = (bmus) => {
  const result = {
    coords: [], bmus
  }
  bmus.forEach(bmu => {
    const loc = parseInt(bmu)
    const x = (loc + grid[0]) % grid[0]
    const y = parseInt((loc - x) / grid[1])
    result.coords.push({ x, y })
  })
  return result
}

const xy_to_bmu = (x, y) => {
  return x + (grid[0] * y)
}

const get_bigger_dimension = () => {
  let result = innerHeight
  if (innerWidth < innerHeight) {
    result = innerWidth
  }
  return result
}

const draw_grid_background = () => {
  stroke('#3f3')
  fill(155)
  rect(0, 0, width, height)

  if (data) {
    /**
     * FROM Stackoverflow
     * https://stackoverflow.com/questions/17313268/idiomatically-find-the-number-of-occurrences-a-given-value-has-in-an-array
     */
    const count = {}
    const dataset = data['bmus']

    dataset.forEach(function (el) {
      count[el[0]] = count[el[0]] + 1 || 1
    });

    Object.keys(count).forEach(key => {
      // need to put this bmu as array [little hack]
      const coords = bmus_to_xy([parseInt(key)])
      const amt = map(count[key], 0, 100, 0, 1)
      const c = lerpColor(color('#0ff'), color('#f00'), amt)
      for (const coord of coords.coords) {
        fill(c)
        square(coord.x * cell_size, coord.y * cell_size, cell_size)
      }
    })
  }
  // noStroke()
  // fill(155)
  // for (let y = 0; y < grid[0]; y++) {
  //   for (let x = 0; x < grid[1]; x++) {
  //     square(x * cell_size, y * cell_size, cell_size)
  //   }
  // }
}

let selected = false
const select_all = (el) => {
  inputs = document.querySelectorAll('.menu-item input')
  if (!selected) {
    inputs.forEach(input => input.checked = true)
    el.textContent = 'deselect all'
  } else {
    inputs.forEach(input => input.checked = false)
    el.textContent = 'select all'
  }
  selected = !selected
  read_inputs()
}




function mouseClicked() {

  draw_grid_background()
  const cnv_measures = cnv.canvas.getBoundingClientRect()

  let x = mouseX
  let y = mouseY
  if (edge(x, y)) {
    x = floor(x / (cell_size))
    y = floor(y / (cell_size))
    // console.log(x, y)
    fill('red')
    rect(x * cell_size, y * cell_size, cell_size, cell_size)

    const indexes = []
    const len = converted_projection_data['projection-bmus'].length
    for (let i = 0; i < len; i++) {

      const bmu = parseInt(converted_projection_data['projection-bmus'][i][0])
      if (bmu === xy_to_bmu(x, y)) {
        indexes.push(i)
      }
    }

    if (indexes.length > 0) display_radio(indexes)
  }
}

const edge = (x, y) => {
  const canvas = cnv.canvas.getBoundingClientRect()
  return (
    x >= 0
    && x <= 0 + canvas.width
    && y >= canvas.top
    && y <= canvas.top + canvas.height
  )
}


function display_radio(indexes) {
  console.log(indexes)
  // draw_data(indexes)
  // const titles = projection_data['Signal_type'][indexes]
  const titles = get_datapoints('Signal_type', indexes)
  // console.log(titles)
  // const text = projection_data['Description'][indexes]
  const texts = get_datapoints('Description', indexes)
  // const audio_path = data['Sample_Audio'][index]
  ///////////////////////////////////////////////////
  const audio_paths = get_datapoints('chunks', indexes)
  ///////////////////////////////////////////////////
  const image_paths = get_datapoints('imgs_path_high', indexes)

  const types_of_signal = []
  for (let i = 0; i < indexes.length; i++) {
    const index = indexes[i]
    types_of_signal.push(known_unknown(index))
  }
  // let type_of_signal = image_path.match(/\.\/(\w*?)\//g)[0]
  // type_of_signal = type_of_signal.match(/\w/g).toString().replace(/\,/g, '')
  const container = document.querySelector('.img-item')
  container.innerHTML = ''

  for (let i = 0; i < titles.unique.length; i++) {

    const index = titles.indexes[i]
    const title = titles.unique[i]
    const text = texts.unique[i]
    ///////////////////////////////////////////////////
    // const audio_path = audio_paths[i].replace('notebooks/signals_archive', 'data')
    const audio_path = `../../study-1/${audio_paths.unique[i]}`
    ///////////////////////////////////////////////////
    // const image_path = image_paths.unique[i].replace('signals_archive', 'data')
    const image_path = `../../study-1/notebooks/${image_paths.unique[i]}`
    // const data_path = image_paths[i][index.position].replace('/high/', '/data_imgs/')
    const type_of_signal = types_of_signal[i]

    const content = document.createElement('div')
    content.setAttribute('class', 'img-content')
    // content.style.backgroundImage = `url(${image_path}), url(${data_path})`
    // const im = image_path.replace('signals_archive', 'data')
    // content.style.backgroundImage = `url(../${image_path})`
    content.setAttribute('data-img_path', image_path)
    content.setAttribute('data-audio_path', audio_path)



    const title_el = document.createElement('div')
    title_el.setAttribute('class', 'img-title')
    title_el.textContent = title
    content.appendChild(title_el)


    // uncomment the below
    // and find solution for the indexes
    const sub_title = document.createElement('div')
    sub_title.setAttribute('class', 'img-subtitle')
    // sub_title.textContent = type_of_signal
    // content.appendChild(sub_title)

    const radio = document.createElement('input')
    radio.type = 'checkbox'
    radio.id = index
    radio.checked = false
    radio.setAttribute('data-index', index)
    radio.setAttribute('data-title', title)
    sub_title.appendChild(radio)

    const label = document.createElement('label')
    label.setAttribute('for', index)
    label.textContent = type_of_signal + ' | ' + index
    sub_title.appendChild(label)
    // here we handle mouse enter and mouse leve to show hide the foreground
    sub_title.addEventListener('mouseenter', () => {
      foreground_bmus(title)
      document.querySelector('.foreground').style.display = 'block'
    })
    sub_title.addEventListener('mouseleave', () => document.querySelector('.foreground').style.display = 'none')
    // sub_title.addEventListener('click', () => )
    content.appendChild(sub_title)


    const description = document.createElement('div')
    description.setAttribute('class', 'img-description')
    const paragraph = document.createElement('p')
    paragraph.innerHTML = text === 'NaN' ? 'No description 🤷🏻‍♀️' : text
    description.appendChild(paragraph)
    content.appendChild(description)

    const audio = document.createElement('audio')
    audio.setAttribute('class', 'audio-element')
    audio.setAttribute('controls', '')
    audio.pause()
    content.appendChild(audio)

    container.appendChild(content)
  }
  // const img_title = document.querySelector('.img-title')
  // const sub_title = document.querySelector('.img-subtitle')
  // const description = document.querySelector('.img-description p')

  // container.style.backgroundImage = `url('../${image_path}')`
  // img_title.textContent = title
  // sub_title.textContent = type_of_signal + ' | ' + index
  // description.textContent = text === 'NaN' ? 'No description 🤷🏻‍♀️' : text

  // // reset audio_player
  // is_playing = false
  // audio_player.pause()

  // audio_player.src = `../${audio_path}`


  /**
   * handling of the lazy load
   * code from: https://medium.com/@ryanfinni/the-intersection-observer-api-practical-examples-7844dfa429e9
   */

  const imgs = document.querySelectorAll('.img-content')
  const handle_intersection = entries => {
    entries.map(entry => {
      if (entry.isIntersecting) {
        entry.target.style.backgroundImage = `url(../${entry.target.dataset['img_path']})`
        const audio_el = entry.target.lastChild
        audio_el.src = `../${entry.target.dataset['audio_path']}`
        observer.unobserve(entry.target)
      }
    })
  }

  const observer = new IntersectionObserver(handle_intersection)
  imgs.forEach(image => observer.observe(image))
}

const get_datapoints = (key, indexes) => {
  // const result = []
  // for (let i = 0; i < indexes.length; i++) {
  //   // const index = indexes[i].index
  //   // result.push(data[key][index])
  //   const index = indexes[i]
  //   result.push(converted_projection_data[key][index])
  // }
  // return result

  const result = []
  for (let i = 0; i < indexes.length; i++) {
    // const index = indexes[i].index
    // result.push(data[key][index])
    const index = indexes[i]
    result.push(converted_projection_data[key][index])
  }

  // return result
  /**
   * Get unique values from array as in
   * https://stackoverflow.com/questions/1960473/
   * get-all-unique-values-in-a-javascript-array-remove-duplicates
   */

  const unique = [...new Set(result)]

  const matching_indexes = {
    unique,
    indexes: []
  }
  for (let i = 0; i < unique.length; i++) {
    const item = unique[i]
    for (let j = 0; j < result.length; j++) {
      if (item === result[j]) {
        matching_indexes.indexes.push(j)
        break
      }
    }
  }
  return matching_indexes
}

const known_unknown = (index) => {

  const image_path = converted_projection_data['Waterfall_image'][index]

  let type_of_signal = image_path.match(/\.\/(\w*?)\//g)[0]
  type_of_signal = type_of_signal.match(/\w/g).toString().replace(/\,/g, '')

  return type_of_signal
}
