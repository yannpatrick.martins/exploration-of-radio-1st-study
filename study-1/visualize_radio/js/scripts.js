
const keys = ["Signal_type", "Description", "Frequency", "Mode", "Modulation", "Bandwidth", "Location", "Sample_Audio", "Waterfall_image", "features", "bmus"]


const paths = {
  _5s: {
    // /study-1/notebooks/signals_archive/chunks_5s/df_chunks_5s_codebook.json
    cb: '../notebooks/signals_archive/chunks_5s/df_chunks_5s_codebook.json',
    data: '../notebooks/signals_archive/chunks_5s/df_chunks_5s_data.json'
  },
  _2_5s: {
    cb: '../notebooks/signals_archive/chunks_2_5s/df_chunks_2_5s_codebook.json',
    data: '../notebooks/signals_archive/chunks_2_5s/df_chunks_2_5s_data.json'
  },
  _1s: {
    cb: '../notebooks/signals_archive/chunks_1s/df_chunks_1s_codebook.json',
    data: '../notebooks/signals_archive/chunks_1s/df_chunks_1s_data.json'
  },
  _5s_silence: {
    cb: '../notebooks/signals_archive/chunks_5s/5s_silence_codebook.json',
    data: '../notebooks/signals_archive/chunks_5s/df_chunks_5s_silence.json'
  },
  _1s_silence: {
    cb: '../notebooks/signals_archive/chunks_1s/1s_silence_codebook.json',
    data: '../notebooks/signals_archive/chunks_1s/df_chunks_1s_silence.json'
  },
  _5s_noise_1: {
    cb: '../notebooks/signals_archive/chunks_5s/5s_noise_1_codebook.json',
    data: '../notebooks/signals_archive/chunks_5s/df_chunks_5s_noise_1.json'
  },
  _5s_noise_2: {
    cb: '../notebooks/signals_archive/chunks_5s/5s_noise_2_codebook.json',
    data: '../notebooks/signals_archive/chunks_5s/df_chunks_5s_noise_2.json'
  },
  topics: './js/topics.json'
}



const populate_load_page = () => {
  const selection = document.querySelector('.select-notebook')
  Object.keys(paths).forEach(key => {
    if (key !== 'topics') {
      const btn = document.createElement('div')
      btn.setAttribute('class', 'btn')
      btn.textContent = key.replace(/[_]/g, ' ')
      btn.addEventListener('click', () => {
        // hide selection
        selection.style.visibility = 'hidden'
        document.querySelector('.load-icon').style.visibility = 'visible'
        init_codebook(paths[key])
      })
      selection.appendChild(btn)
    }
  })
}

populate_load_page()

/**
 * HERE YOU CAN CHANGE THE VARIOUS PATHS
 * 
 */
let path = paths._5s_noise_1


function init_codebook(user_path) {
  path = user_path
  fetch(path.cb)
    .then(res => res.json())
    .then(res => load_codebook(res))
    .catch(err => console.log(err))
}
//////////////////////////////////////////////////

const gutter = 25
let grid = 50
let data
let cell_size = 10
let cnv
let loaded = false


const load_data = path => {
  // console.log(path)
  fetch(path)
    .then(res => res.json())
    .then(res => init_menu(res))
    .catch(err => console.log(err));
}
const imgs = []
let imgs_loaded_in_arr = false
let _100_percent = 0
let show_perc = false
const init_menu = (res) => {
  console.log(res)
  data = res
  /**
   * draw the background once the data is loaded
   */
  draw_grid_background()

  ///////////////////////////////////////////////



  for (let i = 0; i < data['imgs_path_low'].length; i++) {
    const img_paths = data['imgs_path_low'][i]
    for (let j = 0; j < img_paths.length; j++) {
      const path = img_paths[j]
      imgs.push(path)
    }
  }
  _100_percent = imgs.length
  show_perc = true
  imgs_loaded_in_arr = true
  show_all_radio()

  /**
   * here we load the topics into another menu
   */

  fetch(paths.topics)
    .then(res => res.json())
    .then(res => load_topics(res))
    .catch(err => console.error(err))

  // select_all(document.querySelector('.menu'))
}

const load_topics = (res) => {
  console.log(res)
  const left_menu = document.querySelector('.menu-left')
  for (let i = 0; i < res.length; i++) {
    const topic = res[i]
    // console.log(typeof topic)
    // create element
    const keywords = Object.keys(topic)[0]
    const key_div = document.createElement('div')
    key_div.setAttribute('class', 'menu-left-item')
    key_div.textContent = keywords

    // BEWARE THE CONVOLUTED JSON FILE!!!🤢 🤮
    const radio_signals_to_show = topic[keywords].map(item => item.index)
    key_div.addEventListener('click', () => display_bmus(radio_signals_to_show))

    left_menu.appendChild(key_div)
  }


}

const load_codebook = (res) => {
  // console.log(res)
  grid = res.size
  init_canvas(grid)
  load_data(path.data)
}

const range = (start, end) => {
  const result = []
  for (let i = start; i <= end; i++) result.push(i)
  return result
}

const select_inputs = indexes => {
  const inputs = document.querySelectorAll('.menu-item input')
  inputs.forEach(input => {
    input.checked = false
    for (index of indexes) {
      // console.log(input.dataset.index, index)
      if (index === parseInt(input.dataset.index)) {

        input.checked = true
      }
    }
  })
  console.log(indexes, 'select inputs method');
  display_bmus(indexes)
  // show_all_radio()
}

function show_all_radio() {

  /**
   * remove the below soon!
   */



  // const checked = inputs.filter(input => input.checked)
  const index_arr = []
  for (let i = 0; i < data['Signal_type'].length; i++)index_arr.push(i)
  display_bmus(index_arr)
}

const read_inputs = () => {
  console.log('inputs read!');
  const inputs = document.querySelectorAll('.playlist input')
  const titles = []
  inputs.forEach(input => {
    // console.log(input);
    if (input.checked)
      titles.push(input.dataset.title)
  })
  let index_arr = []
  for (let i = 0; i < titles.length; i++) {
    const title = titles[i]
    for (let j = 0; j < data['Signal_type'].length; j++) {
      const signal_type = data['Signal_type'][j]
      if (title === signal_type)
        index_arr.push(j)
    }
    // index_arr = index_arr.concat(range(start, end))
  }
  display_bmus(index_arr)
}

let cnv_sizes

function init_canvas(grid) {
  cell_size = (get_bigger_dimension() - (2 * gutter)) / grid[0];
  resizeCanvas(cell_size * grid[0], cell_size * grid[1]);
  cnv_sizes = cnv.canvas.parentElement.getBoundingClientRect()
}

// function preload() {
//   imgs.push(loadImage('assets/laDefense.jpg'))
// }
function setup() {
  cnv = createCanvas(100, 100)
  cnv.parent('p5')
  background(255)

  noStroke()
  // frameRate(25)
  // noLoop()
  // const path = `data/${dir}/${namespace}/${namespace}_codebook.json`
  // const path = `data/${dir}/${namespace}_codebook.json`
  // const path = paths._2_5s.cb
  // console.log(path)

  /**
   * codebook => data => topics cascading
   */

  // init_codebook()


  cnv.mouseClicked(() => {
    // this is not really good solution because it does not work with images as div...😭
    // console.log('click')
    draw_grid_background()


    const cnv_measures = cnv.canvas.getBoundingClientRect()

    let x = mouseX
    let y = mouseY
    if (edge(x, y, cnv_measures)) {
      x = floor(x / (cell_size))
      y = floor(y / (cell_size))

      fill('red')
      rect(x * cell_size, y * cell_size, cell_size, cell_size)
      const indexes = []
      const len = data['bmus'].length
      for (let i = 0; i < len; i++) {
        let bmus = data['bmus'][i]
        bmus = bmus.reduce((acc, e) => acc.concat(e), [])
        // console.log(bmu);
        for (let j = 0; j < bmus.length; j++) {
          const bmu = bmus[j]
          if (bmu === xy_to_bmu(x, y)) {
            indexes.push({ index: i, position: j })
          }
        }
        // if (bmu[0] === xy_to_bmu(x, y)) {
        //   indexes.push(i)
        // }
      }
      // console.log(indexes);

      if (indexes.length > 0 && !cell_selected) display_radio(indexes)
    }
    // const indexes = data['bmus'].find(index => index[0] === xy_to_bmu(x, y))
    // console.log(xy_to_bmu(x, y));
    // console.log(data['bmus'][100])

    // const signals = data['bmus'].findIndex(datapoint => {
    //   // console.log(datapoint)
    //   // console.log(xy_to_bmu(x, y))
    //   return datapoint[0] === xy_to_bmu(x, y)
    // })
    // // console.log(signals)
    // if (signals !== -1) display_radio(signals)
  })
}

let data_loaded = false
const perc = document.querySelector('#perc')


function draw() {
  // if (loaded) {
  //   print('loaded')
  //   noLoop()
  // }

  if (imgs_loaded_in_arr === true && image_loader_counter >= imgs.length) {
    data_loaded = true
  }

  if (show_perc) {
    const value = floor((image_loader_counter / _100_percent) * 100)

    // perc.textContent = `${floor(image_loader_counter)} of ${_100_percent}`
    perc.textContent = `${value}`
  }

  if (data_loaded) {
    // if (imgs.length === data['imgs_path'].length) {
    loaded = true

    /**
     * remember to uncomment the below to have
     * the whole SOM displayed since beginning
     */

    // show_all_radio()



    // here do spmething to remove intro animation
    document.querySelector('.loading').style.display = 'none'
    console.log('loaded')
    noLoop();
    // }
  }
}

let indexes_to_show = []
const main_container = document.querySelector('.main')
const display_bmus = (index_arr) => {
  indexes_to_show = index_arr

  draw_grid_background()

  let bmus_key = 'bmus'
  Object.keys(data).forEach(key => {
    if (key.startsWith(bmus_key)) {
      bmus_key = key
    }
  })

  const max_arr_length = Math.max(...data[bmus_key].map(el => el.length))
  // console.log(max_arr_length)
  unique_colors(max_arr_length)
  // console.log(colors)



  main_container.innerHTML = ''
  indexes_to_show.forEach(index => {
    const bmus = data[bmus_key][index]
    const type = known_unknown(index)
    display_grid(bmus, type, index)
  })

}

const foreground_bmus = index => {


  const foreground_div = document.querySelector('.foreground')
  foreground_div.innerHTML = ''

  noStroke();
  const bmus = data['bmus'][index]
  /**
   * concat array of arrays following solution from 
   * https://stackoverflow.com/questions/54463977/concat-arrays-in-array-of-objects
   */
  const concat_bmus = bmus.reduce((acc, e) => acc.concat(e), [])
  // console.log(concat_bmus);
  const loc = bmus_to_xy(concat_bmus)
  // different color for different bmu
  // fill(0, 255, 0)

  // stroke(0)
  // colorMode(HSL, 360, 100, 100, 1)
  const inc = 1 / loc.coords.length
  let val = 0
  let i = 0
  for (const coord of loc.coords) {
    // const c = lerpColor(color('#ff3'), color('#33f'), val)
    // make a color function that generates colors on the HSL scale using lightness to switch between light and color to create high contrasts
    // const c = colors[i]
    noFill()
    // // stroke(c.h, c.s, c.l, 1)
    // if (type === 'known') {
    //   stroke('#7f7')
    // } else {
    //   stroke('#77f')
    // }
    // if(i < 100)
    // square(coord.x * cell_size, coord.y * cell_size, cell_size)
    const w = cell_size * 0.85
    const h = w / 2
    const margin = (cell_size - w) / 2
    const path = `../notebooks/${data['imgs_path_low'][index][i]}`
    create_images(path, coord, foreground_div, true)
    // image(imgs[path], (coord.x * cell_size) + margin, cell_size / 4 + (coord.y * cell_size), w, h)

    val += inc
    i++
  }

}

let unique_colors_computed = false
let colors = []
const unique_colors = (length) => {
  if (unique_colors_computed) {
    return
  }

  const increment = 360 / length
  let h = 0
  for (let i = 0; i < length; i++) {
    // H = 0 – 360
    // S = 0 – 100
    // L = 0 – 100
    h += increment
    // const l = i % 2 === 0 ? 20 : 80
    const l = 50
    const s = 100
    colors.push({ h, s, l })
  }

  unique_colors_computed = true
}

const display_grid = (bmus, type, index) => {
  if (bmus) {
    noStroke();
    // console.log(index)
    // console.log(bmus)
    // console.log(type)
    /**
     * concat array of arrays following solution from 
     * https://stackoverflow.com/questions/54463977/concat-arrays-in-array-of-objects
     */
    const concat_bmus = bmus.reduce((acc, e) => acc.concat(e), [])
    // console.log(concat_bmus);
    const loc = bmus_to_xy(concat_bmus)
    // different color for different bmu
    // fill(0, 255, 0)

    // stroke(0)
    // colorMode(HSL, 360, 100, 100, 1)
    const inc = 1 / loc.coords.length
    let val = 0
    let i = 0
    for (const coord of loc.coords) {
      // const c = lerpColor(color('#ff3'), color('#33f'), val)
      // make a color function that generates colors on the HSL scale using lightness to switch between light and color to create high contrasts
      // const c = colors[i]
      noFill()
      // stroke(c.h, c.s, c.l, 1)
      if (type === 'known') {
        stroke('#7f7')
      } else {
        stroke('#77f')
      }
      // if(i < 100)
      square(coord.x * cell_size, coord.y * cell_size, cell_size)
      const w = cell_size * 0.85
      const h = w / 2
      const margin = (cell_size - w) / 2
      const path = `../notebooks/${data['imgs_path_low'][index][i]}`
      create_images(path, coord, main_container, false)
      // image(imgs[path], (coord.x * cell_size) + margin, cell_size / 4 + (coord.y * cell_size), w, h)

      val += inc
      i++
    }
    // colorMode(RGB)
  }
}

let image_loader_counter = 0

const create_images = (path, coord, div, outline) => {
  // console.log(path)
  const left_margin = cnv_sizes.x
  const w = cell_size * 0.85
  const h = w / 2
  const margin = (cell_size - w) / 2
  const img = document.createElement('img')
  img.src = path
  img.style.position = 'fixed'
  img.style.left = left_margin + ((coord.x * cell_size) + margin) + 'px'
  img.style.top = cell_size / 4 + (coord.y * cell_size) + 'px'
  img.style.width = w + 'px'
  img.style.height = h + 'px'

  // when image is loaded increment a conter that tells whether all images have been loaded
  img.onload = () => {
    image_loader_counter++
  }

  if (outline) {
    img.style.outline = '4px solid'
    img.style.outlineColor = '#55ff55'
  }
  div.appendChild(img)
  // for
}

const bmus_to_xy = (bmus) => {
  const result = {
    coords: [], bmus
  }
  bmus.forEach(bmu => {
    const x = (bmu + grid[0]) % grid[0]
    const y = parseInt((bmu - x) / grid[1])
    result.coords.push({ x, y })
  })
  return result
}

const xy_to_bmu = (x, y) => {
  return x + (grid[0] * y)
}

const get_bigger_dimension = () => {
  let result = innerHeight
  if (innerWidth < innerHeight) {
    result = innerWidth
  }
  return result
}

let drawn_once = false
let bg_grid = []
const draw_grid_background = () => {
  stroke('#3f3')
  fill(255)
  rect(0, 0, width, height)
  if (!drawn_once) {

    /**
     * FROM Stackoverflow
     * https://stackoverflow.com/questions/17313268/idiomatically-find-the-number-of-occurrences-a-given-value-has-in-an-array
     */
    const count = {}
    const dataset = []
    for (let i = 0; i < data['bmus'].length; i++) {
      const bmus = data['bmus'][i]
      for (let j = 0; j < bmus.length; j++) {
        const bmu = bmus[j]
        dataset.push(bmu[0])
      }
    }

    dataset.forEach(function (el) {
      count[el] = count[el] + 1 || 1
    })

    const sum = dataset.reduce((previous, current) => current += previous)
    const avg = sum / dataset.length;
    console.log(avg)
    Object.keys(count).forEach(key => {
      // need to put this bmu as array [little hack]
      const coords = bmus_to_xy([parseInt(key)])
      const amt = map(count[key], 0, avg / 2, 0, 1)
      const c = lerpColor(color('#0ff'), color('#f00'), amt)
      for (const coord of coords.coords) {
        fill(c)
        square(coord.x * cell_size, coord.y * cell_size, cell_size)
        bg_grid.push({
          color: c,
          x: coord.x,
          y: coord.y
        })
      }
    })
    drawn_once = true
  } else {
    for (let i = 0; i < bg_grid.length; i++) {
      const cell_value = bg_grid[i]
      fill(cell_value.color)
      square(cell_value.x * cell_size, cell_value.y * cell_size, cell_size)
    }
  }
  // noStroke()
  // fill(155)
  // for (let y = 0; y < grid[0]; y++) {
  //   for (let x = 0; x < grid[1]; x++) {
  //     square(x * cell_size, y * cell_size, cell_size)
  //   }
  // }
}

let selected = false
const select_all = (el) => {
  inputs = document.querySelectorAll('.menu-item input')
  if (!selected) {
    inputs.forEach(input => input.checked = true)
    el.textContent = 'deselect all'
  } else {
    inputs.forEach(input => input.checked = false)
    el.textContent = 'select all'
  }
  selected = !selected
  show_all_radio()
}

let cell_selected = false

// function mouseClicked() {
//   cell_selected = !cell_selected
// }

// function mouseClicked() {
// // 

// draw_grid_background()


// const cnv_measures = cnv.canvas.getBoundingClientRect()

// let x = mouseX
// let y = mouseY
// if (edge(x, y, cnv_measures)) {
//   x = floor(x / (cell_size))
//   y = floor(y / (cell_size))

//   fill('red')
//   rect(x * cell_size, y * cell_size, cell_size, cell_size)
//   const indexes = []
//   const len = data['bmus'].length
//   for (let i = 0; i < len; i++) {
//     let bmus = data['bmus'][i]
//     bmus = bmus.reduce((acc, e) => acc.concat(e), [])
//     // console.log(bmu);
//     for (let j = 0; j < bmus.length; j++) {
//       const bmu = bmus[j]
//       if (bmu === xy_to_bmu(x, y)) {
//         indexes.push({ index: i, position: j })
//       }
//     }
//     // if (bmu[0] === xy_to_bmu(x, y)) {
//     //   indexes.push(i)
//     // }
//   }
//   // console.log(indexes);

//   if (indexes.length > 0 && !cell_selected) display_radio(indexes)
// }
//   // const indexes = data['bmus'].find(index => index[0] === xy_to_bmu(x, y))
//   // console.log(xy_to_bmu(x, y));
//   // console.log(data['bmus'][100])

//   // const signals = data['bmus'].findIndex(datapoint => {
//   //   // console.log(datapoint)
//   //   // console.log(xy_to_bmu(x, y))
//   //   return datapoint[0] === xy_to_bmu(x, y)
//   // })
//   // // console.log(signals)
//   // if (signals !== -1) display_radio(signals)
// }

const edge = (x, y, canvas) => {
  return (
    x >= 0
    && x <= 0 + canvas.width
    && y >= canvas.top
    && y <= canvas.top + canvas.height
  )
}

/**
 * Audio helper function
 */

// const audio_player = document.querySelector('.audio-element')

// is_playing = false

// window.onkeypress = (e) => {
//   if (e.keyCode === 32) {
//     if (is_playing === true) {
//       audio_player.pause()
//       is_playing = !is_playing
//     } else {
//       audio_player.play()
//       is_playing = !is_playing
//     }
//   }
// }

/**
 * this method constructs the vertical playlist of radio signals 
 * 
 * @param {Array} indexes array of index that point to the audio signal analyzed
 */

function display_radio(indexes) {
  console.log(indexes)
  // draw_data(indexes)
  // const titles = data['Signal_type'][indexes]
  const titles = get_datapoints('Signal_type', indexes)

  // const text = data['Description'][index]
  const texts = get_datapoints('Description', indexes)
  // const audio_path = data['Sample_Audio'][index]
  const audio_paths = get_datapoints('chunks', indexes)
  // const image_path = data['Waterfall_image'][index]
  const image_paths = get_datapoints('imgs_path_high', indexes)

  const types_of_signal = []
  for (let i = 0; i < indexes.length; i++) {
    const index = indexes[i].index
    types_of_signal.push(known_unknown(index))
  }
  // let type_of_signal = image_path.match(/\.\/(\w*?)\//g)[0]
  // type_of_signal = type_of_signal.match(/\w/g).toString().replace(/\,/g, '')
  const container = document.querySelector('.img-item')
  container.innerHTML = ''
  let i = 0
  for (const index of indexes) {
    const title = titles[i]
    const text = texts[i]
    const audio_path = audio_paths[i][index.position]
    const image_path = image_paths[i][index.position]
    const data_path = image_paths[i][index.position].replace('/high/', '/data_imgs/')
    const type_of_signal = types_of_signal[i]

    const content = document.createElement('div')
    content.setAttribute('class', 'img-content')

    content.setAttribute('data-img_path', image_path)
    content.setAttribute('data-audio_path', audio_path)
    // content.style.backgroundImage = `url('../../notebooks/${image_path}'), url('../../notebooks/${data_path}')`
    // content.style.backgroundImage = `url('../../notebooks/${image_path}')`

    const title_el = document.createElement('div')
    title_el.setAttribute('class', 'img-title')
    title_el.textContent = title
    content.appendChild(title_el)

    const sub_title = document.createElement('div')
    sub_title.setAttribute('class', 'img-subtitle')
    // sub_title.textContent = type_of_signal + ' | ' + index.index

    const radio = document.createElement('input')
    radio.type = 'checkbox'
    radio.id = index.index
    radio.checked = false
    radio.setAttribute('data-index', index.index)
    radio.setAttribute('data-title', title)
    sub_title.appendChild(radio)

    const label = document.createElement('label')
    label.setAttribute('for', index)
    label.textContent = type_of_signal + ' | ' + index.index
    sub_title.appendChild(label)
    // here we handle mouse enter and mouse leve to show hide the foreground
    sub_title.addEventListener('mouseenter', () => {
      foreground_bmus(index.index)
      document.querySelector('.foreground').style.display = 'block'
    })
    sub_title.addEventListener('mouseleave', () => document.querySelector('.foreground').style.display = 'none')
    // sub_title.addEventListener('click', () => )
    content.appendChild(sub_title)

    const description = document.createElement('div')
    description.setAttribute('class', 'img-description')
    const paragrsph = document.createElement('p')
    paragrsph.textContent = text === 'NaN' ? 'No description 🤷🏻‍♀️' : text
    description.appendChild(paragrsph)
    content.appendChild(description)

    const audio = document.createElement('audio')
    audio.setAttribute('class', 'audio-element')
    audio.setAttribute('controls', '')

    // audio.src = `../${audio_path}`
    audio.pause()
    content.appendChild(audio)

    container.appendChild(content)
    i++
  }

  /**
   * handling of the lazy load
   * code from: https://medium.com/@ryanfinni/the-intersection-observer-api-practical-examples-7844dfa429e9
  */

  const imgs = document.querySelectorAll('.img-content')
  const handle_intersection = entries => {
    entries.map(entry => {
      if (entry.isIntersecting) {
        entry.target.style.backgroundImage = `url('../notebooks/${entry.target.dataset['img_path']}')`
        const audio_el = entry.target.lastChild
        audio_el.src = `../${entry.target.dataset['audio_path']}`
        observer.unobserve(entry.target)
      }
    })
  }

  const observer = new IntersectionObserver(handle_intersection)
  imgs.forEach(image => observer.observe(image))
}

const get_datapoints = (key, indexes) => {
  const result = []
  for (let i = 0; i < indexes.length; i++) {
    const index = indexes[i].index
    result.push(data[key][index])
  }
  return result
}

const known_unknown = (index) => {

  const image_path = data['Waterfall_image'][index]

  let type_of_signal = image_path.match(/\.\/(\w*?)\//g)[0]
  type_of_signal = type_of_signal.match(/\w/g).toString().replace(/\,/g, '')

  return type_of_signal
}