import pickle
import pandas as pd

from tqdm import tqdm
from librosa import display
import librosa
import math
import libs
import libs.fingerprint as fingerprint
from libs.file_reader import FileReader

from termcolor import colored

import sys
import threading

from time import sleep

def main():
    pass 
    data = pickle.load(open('signals_archive/1schunks/df_chunks_1s.pkl', 'rb'))

    data['Sample Audio'] = data['Sample Audio'].str.replace('_study01/testcrop_1s', 'signals_archive/1schunks/testcrop')
    data = data.rename(columns={"Signal type": "Signal_type", "Sample Audio": "Sample_Audio", "Waterfall image": "Waterfall_image"})

    audio_paths = data['Sample_Audio']

    fingerprints = []
    index = 0
    iteration = 0
    # for i in range(iteration, iteration + 1000):

        # if i >= len(audio_paths):
        #     break
    # for i in range(301):
    for filename in audio_paths:
        # filename = audio_paths[i]
        reader = FileReader(filename)
        # # print(index)
        audio = reader.parse_audio()

        for channeln, channel in enumerate(audio['channels']):
            f_p = fingerprint.fingerprint(channel, Fs=audio['Fs'])
        fingerprints.append(f_p)

        sleep(0.5)
        if index % 1000 == 0:
            with open('signals_archive/1schunks/fp_' + str(iteration) + '.p', 'wb') as _file:
                pickle.dump(fingerprints, _file)
            fingerprints = []
        print(index)
        index += 1
            
sys.setrecursionlimit(500000000)    # adjust numbers
threading.stack_size(13421772800)   # for your needs

main_thread = threading.Thread(target=main)
main_thread.start()
main_thread.join()
