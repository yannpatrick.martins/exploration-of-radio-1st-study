from pydub import AudioSegment
from pydub.utils import make_chunks
import os, glob, re
import pandas as pd
import numpy as np
import librosa
import librosa.display
import pickle

# define home folder
# home = '/home/yann/yann/organizing' # valid for the jupyter server

'''
MAKE SURE THIS SCRIPT IS RUN FROM ROOT
'''

def crop_audio(df, duration, home, folder_name):
    '''
    get audio files from dataFrame and chunking audio files longer than [duration]
    copying the rest of the row's content for each chunk

    Parameters
    --------------
    df : pd.dataFrame
        dataframe from where we get the audio files
    duration : int
        duration of the chunks of audio in milliseconds
    home : str
        path of home folder
    folder_name : str
        name of the folder that needs to be created to save the audio chunks
    '''


    '''
    IS THIS EVEN USEFUL?
    '''

    # create empty dataframe, copying the columns from original
    # dfResampled = pd.DataFrame(columns=df.columns)

    # window_size = 1024 # see frequency, (narrowband, 1024) - see transitions timing (wideband, 32)
    # window = np.hanning(window_size)

    '''
    ////////////////////////////////////
    '''


    audio_folder = 'audio'

    # create folders if not existing already
    path = os.path.join(home, folder_name)
    if not os.path.isdir(path):
        os.mkdir(path)

    path = os.path.join(path, audio_folder)
    if not os.path.isdir(path):
        os.mkdir(path)


    chunks_paths = []
    for i in df.index:
        row = df.loc[i] # store the row as a pd.series
        audio_file = df['Sample_audio'][i]
        source_name, source_format = os.path.splitext(path_leaf(audio_file))
        audio_file = audio_file.replace('./', home + '/')
        # print('//////////////////////////')
        # print(audio_file)
        audio = AudioSegment.from_file(audio_file, source_format[1:])
        chunks = make_chunks(audio, duration)
        audio_paths = []
        for n, chunk in enumerate(chunks):
            chunk_name = ''
            if len(chunk) == duration: # only keep the chunks that are of the same length
                chunk_name = home + '/' + folder_name + '/audio/' + source_name + ("_{0}.wav".format(n))
            else:
                while len(chunk) < duration:
                    chunk += chunk
                chunk = chunk[:duration]
                chunk_name = home + '/' + folder_name + '/audio/' + source_name + '_R.wav' ### HERE CHANGE FILE PATHS
            audio_paths.append(chunk_name)
            # export audio as wav
            chunk.export(chunk_name, format="wav") # export the file to new folder
        chunks_paths.append(audio_paths)
    df['chunks'] = chunks_paths

    with open(home + '/' + folder_name + '/df_' + folder_name + '.pkl', 'wb') as _file:
        pickle.dump(df, _file)

def path_leaf(path):
    head, tail = os.path.split(path)
    return tail or os.path.basename(head)

# 
def convert_to_number(frq):
    '''
    convert frequency and bandwidth value into floats (Yann's regex as function)

    Parameters
    -----------
    frq : str
        frequency expressend as a using the Mhz, Khz, hz standard

    Returns
    ----------
    int
        the frequency is converted to its numerical value: 1Mhz = 1000000
    '''
    if isinstance(frq, str):
        frq = re.sub(r'[^\.\S]', '',frq)
        parsed_char = re.sub(r'\d', '',frq)
        parsed_char = re.sub(r'\W', '',parsed_char)
        parsed_val = re.sub(r'[^\.\d]', ' ',frq)
        # print(parsed_val)
        # parsed_val = re.findall(r'\d+', parsed_val)
        parsed_val = re.split(' ', parsed_val)
        while("" in parsed_val) :
            parsed_val.remove("")
        mult = parsed_char
        value = parsed_val
        if len(value) > 1:
            value = (float(value[0]) + float(value[1])) / 2
        else:
            value = float(value[0])
        if mult[0] == 'M':
            mult = 1000000
        elif mult[0] == 'k':
            mult = 1000
        else:
            mult = 1
    else:
        value = 0
        mult = 1
    new_value=value*mult
    return new_value



home = 'notebooks/signals_archive' 
folder_name = 'chunks_30s'
duration = 30000 # duration in milliseconds​



'''
THIS BELOW imports and cleans the dataset
'''
csvs = glob.glob(home + "/*.csv")
df_from_each_file = (pd.read_csv(f, error_bad_lines=False) for f in csvs)
df = pd.concat(df_from_each_file, ignore_index=True, sort=False)
# remove rows where there is no audio sample:
# replace content of cells marked '—','No Audio File','Html5mediator: file extension not recognized' with NaN
# other lines contain valid files
df['Sample Audio'].replace('—', 'NaN', inplace=True)
df['Sample Audio'].replace('No Audio File', 'NaN', inplace=True)
df['Sample Audio'].replace('Html5mediator: file extension not recognized', 'NaN', inplace=True)
df.dropna(subset=['Sample Audio'], inplace=True)
df=df.drop(columns=['id'])
df=df.reset_index(drop=True)



# apply function to the Frequency and Bandwidth cells in the dataframe
df['Frequency'] = df['Frequency'].apply(convert_to_number)
df['Bandwidth'] = df['Bandwidth'].apply(convert_to_number)
df=df.rename(columns={"Signal type": "Signal_type", "Sample Audio": "Sample_audio", "Waterfall image": "Waterfall_image"})
df=df.reset_index(drop=True) # reset index column so numbers are unique and in ascending order
'''
////////////////////////////////////////////
'''

# CROP FILES

crop_audio(df, duration, home, folder_name)
# crop_audio(df, 2500, home, 'chunks_2_5s')
# crop_audio(df, 1000, home, 'chunks_1s')