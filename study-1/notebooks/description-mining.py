import pickle
import pandas as pd
import numpy as np
import json

# load dataframe
path_chunks_5s = '/Users/yannpatrickmartins/Documents/IXDM/exploration_of_radio/utils/1st-study/notebooks/signals_archive/chunks_5s/df_chunks_5s.pkl'
path_chunks_2_5s = 'signals_archive/chunks_2_5s/df_chunks_2_5s.pkl'
path_chunks_1s = 'signals_archive/chunks_2_5s/df_chunks_1s.pkl'
path_chunks = path_chunks_5s
data = pickle.load(open(path_chunks, 'rb'))

# replace NaN descriptions with ''
data.Description = data.Description.fillna('')

# make LDA model
from sklearn.feature_extraction.text import CountVectorizer
cvec = CountVectorizer().fit(data.Description)
from sklearn.feature_extraction import text as TXT
radio_stop_words = ['hz', 'hertz', 'khz', 'hzhertz', 'kilohertz', 'khzkilohertz', 'mhz', 'megahertz', 'mhzmegahertz']
stopwords = TXT.ENGLISH_STOP_WORDS.union(radio_stop_words)

from sklearn.decomposition import NMF, LatentDirichletAllocation, TruncatedSVD

NUM_TOPICS = 10
vectorizer = CountVectorizer(min_df=5, max_df=0.9,
                             stop_words=TXT.ENGLISH_STOP_WORDS.union(radio_stop_words), lowercase=True,
                             token_pattern='[a-zA-Z\-][a-zA-Z\-]{2,}')
descriptions_vectorized = vectorizer.fit_transform(data.Description.tolist())

lda_model = LatentDirichletAllocation(n_components=NUM_TOPICS, max_iter=10, learning_method='online')
lda_Z = lda_model.fit_transform(descriptions_vectorized)
print(lda_Z.shape)  # (NO_DOCUMENTS, NO_TOPICS)

no_top_words = 12
no_top_documents = 10
lda_H = lda_model.components_
tf_feature_names = vectorizer.get_feature_names()

out = []

def display_topics(H, Z, feature_names, docs, no_top_words, no_top_documents):
    for idx, topic in enumerate(H):

        tmp_dict = {}

        # print("%d:" % (idx), " ".join([feature_names[i] for i in topic.argsort()[:-no_top_words - 1:-1]]))
        curr_topic = " ".join([feature_names[i] for i in topic.argsort()[:-no_top_words - 1:-1]])
        # print(curr_topic)
        tmp_dict[curr_topic] = []
        # checking which signals are the most characteristic for certain topics

        top_doc_indices = np.argsort( Z[:,idx] )[::-1][0:no_top_documents]
        for i in range(5):

            signal_index=top_doc_indices[i]
            # print(signal_index, ":", docs[signal_index])
            
            inner_tmp_dict = {
                "index": signal_index,
                "name": docs[signal_index]
            }

            tmp_dict[curr_topic].append(inner_tmp_dict)
        out.append(tmp_dict)

display_topics(lda_H, lda_Z, tf_feature_names, data.Signal_type, no_top_words, no_top_documents)

print(out)

def convert(o):
    '''
        workaround from stackoverflow to prevent error
        when having data from numpy
    '''
    # if isinstance(o, int64): return int(o)
    if isinstance(o, np.int64): return int(o)
    if isinstance(o, np.float64): return float(o)
    if isinstance(o, np.ndarray): return o.tolist()
    raise TypeError

parse_to_json = json.dumps(out, default=convert)
with open('visualize_radio/js/topics.json', 'w') as f:
    f.write(parse_to_json)
