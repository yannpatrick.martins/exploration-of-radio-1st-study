
import os
from pydub import AudioSegment
from pydub.utils import audioop
import numpy as np
from hashlib import sha1


class BaseReader(object):
    def __init__(self, a):
        self.a = a

    def recognize(self):
        pass  # base class does nothing


class FileReader(BaseReader):
    def __init__(self, filename):
        # super(FileReader, self).__init__(a)
        self.filename = filename

    """
  Reads any file supported by pydub (ffmpeg) and returns the data contained
  within. If file reading fails due to input being a 24-bit wav file,
  wavio is used as a backup.
  Can be optionally limited to a certain amount of seconds from the start
  of the file by specifying the `limit` parameter. This is the amount of
  seconds from the start of the file.
  returns: (channels, samplerate)
  """
    # pydub does not support 24-bit wav files, use wavio when this occurs

    def parse_audio(self):
        limit = None
        # limit = 5

        songname, extension = os.path.splitext(os.path.basename(self.filename))

        try:
            audiofile = AudioSegment.from_file(self.filename)

            if limit:
                audiofile = audiofile[:limit * 1000]

            data = np.fromstring(audiofile._data, np.int16)

            channels = []
            for chn in range(audiofile.channels):
                channels.append(data[chn::audiofile.channels])

            fs = audiofile.frame_rate
        except audioop.error:
            print('audioop.error')
            pass
            # fs, _, audiofile = wavio.readwav(filename)

            # if limit:
            #     audiofile = audiofile[:limit * 1000]

            # audiofile = audiofile.T
            # audiofile = audiofile.astype(np.int16)

            # channels = []
            # for chn in audiofile:
            #     channels.append(chn)

        return {
            "songname": songname,
            "extension": extension,
            "channels": channels,
            "Fs": audiofile.frame_rate
        }
