# Architectonig Exploration of Radio – Code Base
## TO DO:

* make a proper README with intro

* unify the `.css` files

* make a global `.js` with all the helper function shared among the 2 studies

* clean up the study-2 notebooks

* add links to Sigidwiki, and other sources for our database

* add `choose dataset...` in study-2

* add `LICENSE`